package be.beeworks.streaming.tweet.mapper;

import be.beeworks.streaming.tweet.TweetDto;
import be.beeworks.streaming.tweet.model.Tweet;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class TweetMapperTest {
    private TweetMapper tweetMapper;
    private String testAuthor;
    private String testText;
    private String testId;
    private Date testDate;
    private List<String> testHashtags;

    @Before
    public void setup() {
        tweetMapper = new TweetMapper();
        testId = UUID.randomUUID().toString();
        testAuthor = UUID.randomUUID().toString();
        testText = UUID.randomUUID().toString();
        testHashtags = Arrays.asList("test","tweet","kafka");
        testDate = new Date();
    }

    @Test
    public void testTweetToDto() {
        TweetDto tweetDto = tweetMapper.getTweetDto(new Tweet(testId, testText, testAuthor, testDate, testHashtags));
        assertEquals(testId, tweetDto.getId());
        assertEquals(testAuthor, tweetDto.getAuthor());
        assertEquals(testText, tweetDto.getText());
        assertEquals((Long)testDate.getTime(), tweetDto.getTimestamp());
        assertEquals(new HashSet<String>(testHashtags), new HashSet<String>(tweetDto.getHashtags()));
    }

    @Test
    public void testDtoToTweet() {
        Tweet tweet = tweetMapper.getTweet(TweetDto.newBuilder().setId(testId).setAuthor(testAuthor).setText(testText).setTimestamp(testDate.getTime()).setHashtags(testHashtags).build());
        assertEquals(testId, tweet.getId());
        assertEquals(testAuthor, tweet.getAuthor());
        assertEquals(testText, tweet.getText());
        assertEquals(testDate, tweet.getTimestamp());
        assertEquals(new HashSet<String>(testHashtags), new HashSet<String>(tweet.getHashtags()));
    }
}
