package be.beeworks.streaming.tweet.input;

import be.beeworks.streaming.tweet.model.Tweet;

public interface TweetReceiver {
    void receive(Tweet tweet);
}
