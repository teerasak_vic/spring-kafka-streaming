package be.beeworks.streaming.tweet.input;

import be.beeworks.streaming.tweet.TweetDto;
import be.beeworks.streaming.tweet.mapper.TweetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;

public class TweetEventListener {
    @Autowired(required = false)
    private TweetReceiver tweetReceiver;

    @Autowired
    private TweetMapper tweetMapper;

    @StreamListener(TweetInput.TWEET_INPUT)
    public void receiveTweet(TweetDto tweetDto) {
        if (tweetReceiver != null) tweetReceiver.receive(tweetMapper.getTweet(tweetDto));
    }
}
