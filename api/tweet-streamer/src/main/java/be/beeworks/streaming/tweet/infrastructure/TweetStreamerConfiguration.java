package be.beeworks.streaming.tweet.infrastructure;

import org.springframework.cloud.stream.schema.avro.AvroSchemaMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.util.MimeType;

import java.io.IOException;

@Configuration
@PropertySource("classpath:/tweets.properties")
public class TweetStreamerConfiguration {

    @Bean
    public MessageConverter tweetMessageConverter() throws IOException {
        AvroSchemaMessageConverter avroSchemaMessageConverter = new AvroSchemaMessageConverter(MimeType.valueOf("application/tweet.v1+avro"));
        avroSchemaMessageConverter.setSchemaLocation(new ClassPathResource("avro/TweetDto.v1.avsc"));
        return avroSchemaMessageConverter;
    }

}
