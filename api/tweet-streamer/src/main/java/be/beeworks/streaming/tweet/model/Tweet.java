package be.beeworks.streaming.tweet.model;

import java.util.*;

public class Tweet {
    private final String id;
    private final String text;
    private final String author;
    private final Date timestamp;
    private final Set<String> hashtags;

    public Tweet() {
        this(UUID.randomUUID().toString(),null,null,new Date(),new HashSet<>());
    }

    public Tweet(String id, String text, String author, Date timestamp, Collection<String> hashtags) {
        assert(id != null);
        assert(timestamp != null);
        assert(hashtags != null);
        this.id = id;
        this.text = text;
        this.author = author;
        this.timestamp = timestamp;
        this.hashtags = Collections.unmodifiableSet(new HashSet<>(hashtags));
    }

    public Tweet withHashtag(String hashtag) {
        return withHashtags(Arrays.asList(hashtag));
    }

    public Tweet withHashtags(Collection<String> hashtagsToAdd) {
        Set<String> hashtags = new HashSet<>();
        hashtags.addAll(this.hashtags);
        if (hashtagsToAdd != null) hashtags.addAll(hashtagsToAdd);
        return new Tweet(this.id, this.text, this.author, this.timestamp, hashtags);
    }

    public Tweet withText(String text) {
        return new Tweet(this.id, text, this.author, this.timestamp, hashtags);
    }

    public Tweet withAuthor(String author) {
        return new Tweet(this.id, this.text, author, this.timestamp, hashtags);
    }

    public Tweet withId(String id) {
        return new Tweet(id, this.text, this.author, this.timestamp, hashtags);
    }

    public Tweet withTimestamp(Date timestamp) {
        return new Tweet(this.id, this.text, this.author, timestamp, hashtags);
    }

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getAuthor() {
        return author;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public Set<String> getHashtags() {
        return hashtags;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Tweet{");
        sb.append("id='").append(id).append('\'');
        sb.append(", text='").append(text).append('\'');
        sb.append(", author='").append(author).append('\'');
        sb.append(", timestamp=").append(timestamp);
        sb.append(", hashtags=").append(hashtags);
        sb.append('}');
        return sb.toString();
    }
}
