package be.beeworks.streaming.tweet.input;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface TweetInput {
    String TWEET_INPUT = "tweetInput";

    @Input(TWEET_INPUT)
    SubscribableChannel tweetInput();
}
