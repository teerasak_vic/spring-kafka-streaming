package be.beeworks.streaming.tweet;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

@Component
public class AuthorGenerator {
    private final List<String> authors;
    private final Random random = new Random();


    public AuthorGenerator() {
            authors = new ArrayList<>();
            final Lorem lorem = LoremIpsum.getInstance();
            IntStream.range(0,10).forEach(number -> authors.add(lorem.getName()));
    }

    public String randomAuthor() {
        return authors.get(random.nextInt(authors.size()));
    }
}
