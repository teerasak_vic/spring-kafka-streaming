package be.beeworks.streaming.tweet;

import be.beeworks.streaming.tweet.output.TweetOutput;
import be.beeworks.streaming.tweet.output.TweetSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@ComponentScan("be.beeworks.streaming.tweet")
@EnableBinding(TweetOutput.class)
@EnableScheduling
public class Application {
    @Bean
    public TweetSender tweetWriter() {
        return new TweetSender();
    }

    @Autowired
    private TweetGenerator tweetGenerator;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Scheduled(initialDelay = 5000, fixedDelay = 1000)
    public void generateTweet() {
        tweetGenerator.generateTweet();
    }
}
