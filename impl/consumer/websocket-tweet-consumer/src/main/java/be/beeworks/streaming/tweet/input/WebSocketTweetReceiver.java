package be.beeworks.streaming.tweet.input;

import be.beeworks.streaming.tweet.model.Tweet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class WebSocketTweetReceiver implements TweetReceiver {
    private static final Logger logger = LoggerFactory.getLogger(WebSocketTweetReceiver.class);

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Override
    public void receive(Tweet tweet) {
        logger.info("Received tweet: {}", tweet);
        simpMessagingTemplate.convertAndSend("/topic/tweets", tweet);
    }
}
