var stompClient = null;

function connect() {
    var socket = new SockJS('/tweet-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/tweets', function (entry) {
            showEntry(JSON.parse(entry.body));
        });
    });
}

function showEntry(entry) {
    var authorId = toId(entry.author);
    var panelId = toId(entry.text);
    addColumn(authorId, entry.author);
    $("#" + authorId).prepend("<div id='" + panelId + "' class='panel panel-default'><div class='panel-heading'>" + entry.author + "</div><div class='panel-body'>" + entry.text + "</div><div class='panel-footer' id='" + panelId + "Footer'> </div> </div>");
    $("#" + panelId).effect("highlight","slow");
}

function addColumn(authorId, title) {
    if ( !$( "#" + authorId ).length ) {
        $("#deck").append("<div class='col-xs-4'><h2>"+ title + "</h2><div id='" + authorId + "'></div></div>");
    }
}

function toId(text) {
    return text.replace(/\W+/g, "");
}

$(function () {
    connect();
});